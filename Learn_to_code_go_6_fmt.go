// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

var a int
var b string = "Something here"
var c bool
var d bool = true

func main() {
	// e := 21
	// f := "Something else is here"
	// g := `This statement shows how its different from "Something here"`
	// h := `Another literal statement
	// "with something in it"`

	// fmt.Println(a)
	// fmt.Println(b)
	// fmt.Println(c)
	// fmt.Println(d)
	// fmt.Println(e)
	// fmt.Println(b, "Extra addition", f)
	// fmt.Println(f)
	// fmt.Println(g)
	// fmt.Println(h)

	// fmt.Println("Or it can be printed easily like")

	fmt.Printf("%v\n", a)  // format printing
	fmt.Printf("%v\n", b)  // \n gives the new line as it is not Printf
	fmt.Printf("%d\n", a)  // for ints, can be used too
	fmt.Printf("%#v\n", a) // # gives string in ""
	fmt.Printf("%#v\n", b)
	fmt.Printf("%T\n", a) // gives the type of the variable
	fmt.Printf("%T\n", b)

	fmt.Printf("%v\t%v\n", a, b) // tab instead of new line

	s := fmt.Sprintf("%v\t%v", a, b) // prints the output in string format
	fmt.Println(s)

}
