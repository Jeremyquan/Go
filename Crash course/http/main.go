package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

// checking out default interfaces from stf libraries
// reader interface is to take nay kin of inpot and show it as a byte slice

// custom type to implement write interface and pass it to the copy fucntion

type logWriter struct{}

func main() {
	resp, err := http.Get("https://google.com/") // get gets a pointer and an error
	if err != nil {
		fmt.Println("Error", err) // show the error and
		os.Exit(1)                //  exit
	}
	// fmt.Println(resp) // generally in other languages, this would give the result but not in go

	// // byte slice to store the data coming in from the read function
	// // this way is not practical and instead helper fucntions are used which are in built
	// bs := make([]byte, 99999) // another way of making a slice with builtin space for storage
	// resp.Body.Read(bs)
	// fmt.Println(string(bs))

	// // lesser code way of getting the body
	// fmt.Println("*********************")
	// this uses writer interface which takes some byte slice and writes it out
	// io.Copy(os.Stdout, resp.Body) // copy can be seen as function that takes something from outside and copies it to outside
	// this needs a lot of going through document
	fmt.Println("******using custom writer function")
	lw := logWriter{} // this implemetnts writer interface as it have write function
	io.Copy(lw, resp.Body)
}

// function that works with logwriter type
func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	return len(bs), nil // from documnetations
}
