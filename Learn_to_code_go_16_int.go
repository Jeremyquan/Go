// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
	"runtime"
)

// int and float
// uint is unsigned int, so only positive numbers
// int 8 is 2^8 from - to + 127
// uint8 is 2^8 from 0 to 255  <- also called as byte
// rune is int32 <- utf-8 coding scheme

// int and unit decides to optimize based on the system architecture 	<- mechanical sympathy

var a int
var b float64

func main() {

	x := 43
	y := 43.34
	fmt.Println(x, y)
	fmt.Printf("%T\n%T\n", x, y)

	fmt.Println(a, b)
	fmt.Printf("%T\n%T\n", a, b)

	z := int(y) // conversion
	fmt.Println(z)
	fmt.Printf("%T\n", z)

	// OS and acrchitecture info

	fmt.Println(runtime.GOOS)
	fmt.Println(runtime.GOARCH)

}

/*

43 43.34
int
float64
0 0
int
float64
43
int
linux
amd64

*/
