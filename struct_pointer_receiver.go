package main

import "fmt"

// there are two types of struct/methods/function
// value receivers and pointer receivers

const usixteenbitmax float64 = 65535
const kmh_multiple float64 = 1.60934



type car struct {
	gas_peddle uint16  //0-65535 unsigned int
	brake_peddle uint16
	sterring_wheel int16 //-32k -32k
	top_speed_kmh float64
}

// fcuntion to calulate kmh (value receiver)
func (c car) kmh() float64{   // car is the datatype and c is assoiciated and represented
	return float64(c.gas_peddle)*(c.top_speed_kmh/usixteenbitmax)

} 


func (c car) mph() float64{   // car is the datatype and c is assoiciated and represented
	return float64(c.gas_peddle)*(c.top_speed_kmh/usixteenbitmax)/(kmh_multiple)

} 

func (c *car) new_top_speed(new_speed float64) {// reading through car type to get the pointer
	c.top_speed_kmh = new_speed
}



func main(){
	a_car := car{65000, 0, 12561, 225.0} // create a car with these parameters
	fmt.Println(a_car.gas_peddle)
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())
	a_car.new_top_speed(500) // modifed using pointer receiver
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())

}