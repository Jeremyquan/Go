package main

import "fmt"

func str(a, b string) (string, string) { // specify both the types of output
	return a, b
}

func main() {
	var a, b string = "Hello", "!"
	fmt.Println(str(a, b))
}

//each and every variable and imported lib has to be used
