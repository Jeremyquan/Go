// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

var x int = 42
var y string = "James Bond"
var z bool = true

func main() {

	s := fmt.Sprintf("%v\t%v\t%v\t", x, y, z)
	fmt.Println(s)

}

/*

42	James Bond	true


*/
