/*

 */

package main

import (
	"fmt"
)

// Enumerated constants
// Generally used at package level
const (
	b = iota // counter used to create enumerated constant
	c = iota
	d = iota
	e // this can be left without assigning and still get iota
	f
)

const (
	g = iota // this wil reset the iota since its in a different block
)

// since iota starts with 0 and it has to be ignored
const (
	_ = iota
	c2
	d2
	e2
	f2
)

// the initialization of iota can be altered

const (
	x = iota + 5
	y
	z
)

func main() {
	fmt.Printf("%T\n%v\n", b, b)
	fmt.Printf("%T\n%v\n", c, c)
	fmt.Printf("%T\n%v\n", d, d)
	fmt.Printf("%T\n%v\n", e, e)
	fmt.Printf("%T\n%v\n", f, f)
	fmt.Printf("%T\n%v\n", g, g)
	// Naming convention for constants
	// Camel casing, unless exported, then first letter is upper case
	const aConst int = 42 // typed constant
	fmt.Printf("%T\n%v\n", aConst, aConst)

	// A constant cant be reassigned like vars
	// constant has to be assignable at compile time
	// They can be of any primitive types
	// Some types are mtable liek arrays so they cant be a constant
	// Constant also cant be shadowed, the one inside the main will be used

	// Constant can be operated with anykind no matter what kind it is
	const a = 32
	var h int16 = 2
	fmt.Printf("%T\n%v\n", a+h, a+h) // this sees a as 32 literal and gives pass, implicit conversions

	fmt.Println(x, y, z)
}

/*

int
0
int
1
int
2
int
3
int
4
int
0
int
42
int16
34
5 6 7
*/
