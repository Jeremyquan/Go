/*
Primitives
> Boolean
> Numeric : Int, FLoat, Complex
> Text

*/

package main

import (
	"fmt"
)

func main() {
	x := true
	fmt.Printf("%v, %T\n", x, x)

	// Boolean can be used to check the state of something
	// It can also be used to do logical tests

	n := 1 == 1 // equaality check
	m := 1 == 2
	fmt.Printf("%v, %T\n", n, n)
	fmt.Printf("%v, %T\n", m, m)

	// Zero value, a value assigned to anything declared before assignment
	var y bool
	fmt.Printf("%v, %T\n", y, y) // Zero value of a bool is false

	// Numeric tyeps
	var a int                    // integer of unspecifed size atleast 32 bits, depending on the system
	fmt.Printf("%v, %T\n", a, a) // Zero value of a int is 0
	// there are int8, int 16, int 32, int64 from neg to positive
	// anything more than this must use "big" package from math library

	var b uint16                 // unsigned integer, which are always from 0 to some size similar to signed
	fmt.Printf("%v, %T\n", b, b) // Zero value of a uint is 0
	// there is no unsigned 64 but we have a type byte

	// Dividing an int by int will give back an int
	// operations can be on similar type

	// Bit operator
	c := 2
	d := 5
	fmt.Println(c & d)  //and
	fmt.Println(c | d)  // or
	fmt.Println(c ^ d)  // xor
	fmt.Println(c &^ d) // nand

	// these work on the binary for of c and d to give the output

	// bit shifting
	e := 6
	fmt.Println(e << 3) // shifts bit of 6 by 3 left
	fmt.Println(e >> 3) // shifts bit of 6 by 3 right

	// FLoating type
	// IEEE74 std is being followed, 32 and 64
	f := 3.14
	f = 13.7e32
	f = 2.1E12
	fmt.Printf("%v\n%T\n", f, f)

	// Complex numbers
	var g complex64 = 1 + 2i
	var h complex128 = 1.5 + 30i
	fmt.Printf("%v\n%T\n", g, g)
	fmt.Printf("%v\n%T\n", real(g), real(g))
	fmt.Printf("%v\n%T\n", imag(g), imag(g))
	fmt.Printf("%v\n%T\n", h, h)
	fmt.Printf("%v\n%T\n", real(h), real(h))
	fmt.Printf("%v\n%T\n", imag(h), imag(h))
	fmt.Println(g * complex64(h))
	fmt.Println(g + complex64(h))
	fmt.Println(g - complex64(h))
	fmt.Println(g / complex64(h))

	var i complex64 = complex(5, 12)
	fmt.Printf("%v\n%T\n", i, i)

	// Text type
	// string (utf8 character only) and ...
	var j string = "Something"
	fmt.Printf("%v\n%T\n", j, j)
	fmt.Println(j[3:5])                // parts of string
	fmt.Printf("%v\n%T\n", j[0], j[0]) // gives something strange
	fmt.Printf("%v\n%T\n", string(j[0]), string(j[0]))
	// strings in go are alias for bytes, which are immutable so string cant be manipulated
	// string concatenation
	k := "is here"
	fmt.Println(j + k)

	// slice of bytes are collection of bytes
	l := []byte(k)
	fmt.Printf("%v\n%T\n", l, l) // gives slice/list of uint8, ascii or utf value

	// go uses byte slices extensively than hardcoded string
	// whens tring or fiels are sent around, the byte of slice is way to do it

	// RUNE
	// string but uses UTF32 character
	var o rune = 'a'             // not a string as string comes in double quotes
	fmt.Printf("%v\n%T\n", o, o) // returns a int32, runes are type alias

}

/*

true, bool
true, bool
false, bool
false, bool
0, int
0, uint16
0
7
7
2
48
0
2.1e+12
float64
(1+2i)
complex64
1
float32
2
float32
(1.5+30i)
complex128
1.5
float64
30
float64
(-58.5+33i)
(2.5+32i)
(-0.5-28i)
(0.068162926-0.029925186i)
(5+12i)
complex64
Something
string
et
83
uint8
S
string
Somethingis here
[105 115 32 104 101 114 101]
[]uint8
97
int32


*/
