/*


 */

package main

import (
	"fmt"
	"strconv" // string conversion package
)

// variables can also be declared at package level
// this method cant have := in it
var x float32 = 42

// fmt.Println("The package level x is ",x) <- this cant be done outside the main

// variables can be declared in a block too, to make code clear and consice
var (
	y string = "Something is here"
	z bool   = true
)

// this way of declaring at the package level in block can show how these variables can be related to each other

func main() {
	fmt.Println("The package level x is ", x)
	// three ways to declare a variable, and any declared variable has to be used or else there will be errors
	// 1. initialse but not used variable
	var i int                    // variable i which is of integer type, type the way you say it
	fmt.Println(i)               // to display the zero type, default value of un initiailised variable
	fmt.Printf("%T\n%v\n", i, i) // formatting string , v = value, T= type

	// 2. When type is not clear to the go compiler
	var j int = 42
	fmt.Println(j)

	// 3.
	k := 42 // go compiler assumes the type
	fmt.Printf("%T\n", k)

	l := 42. // to make floating value, but it assumes float64, so to avoid that use 2. method
	fmt.Printf("%T\n", l)

	fmt.Printf("%T\n%v\n%T\n%v\n%T\n%v\n", x, x, y, y, z, z)

	// a value already declared cant be declared using := again, although it can be reassigned
	j = 32
	// j = "String instead of int" but cant change the type while reassigning
	fmt.Printf("The new j type is %T\nThe new j value is %v\n", j, j)

	// Shadowing
	// Variables declared at package leve can be re-declared in the main as the inner most scope takes precedence
	var x float32 = 32
	fmt.Printf("The inner x type is %T\nThe inner x value is %\n", x, x)

	// Naming of variables
	// The variables declared in lower case will be invisible to the package and is scoped to that
	// If in upper case, then the variable is exposed to the outside world
	// Block scope: used and visible isde the block ex: func main

	// Naming convention
	// Length of the variable name should reflect the life of the variable
	// The variables which are used to for a longer time, should have clear name so that anyone outside the source code can understand
	// For readablity, the acronymns in variable names are supposed to be upper case

	// Conversion of variable type
	w := float64(x)
	fmt.Printf("%T\n%v\n", w, w) // Conversion can lose information while converting float to int
	// So go automatically does not allow that unless explicitly mentioned

	// Converting int to string
	u := 23
	s := string(u)
	fmt.Printf("%T\n%v\n", s, s) // the converted string will be as per unicode character

	t := strconv.Itoa(u) // I to a, int to ascii
	fmt.Printf("%T\n%v\n", t, t)
}

/*
The package level x is  42
0
int
0
42
int
float64
float32
42
string
Something is here
bool
true
The new j type is int
The new j value is 32
The inner x type is float32
The inner x value is %!
(float32=32)float64
32
string

string
23

*/
