package main

import (
	"fmt"
)

func main() {

	// Syntax 1
	if false {
		fmt.Println("ITs true")
	} else {
		fmt.Println("ITs false")
	}

	// Syntax 2 : Initializer syntax
	statePop := make(map[string]int)
	statePop = map[string]int{
		"TN": 12,
		"AP": 10,
		"KT": 13,
		"KL": 10,
	}
	if pop, ok := statePop["AP"]; ok { // initializer before ;
		fmt.Println(pop)
	}

	// Syntax 3: Non boolean
	a := 3
	b := 3
	c := false
	if a < b || a > b { // in or statement if one is true go skips the other and takes a shortcut
		fmt.Println("not same")
	}

	if a == b && !c {
		fmt.Println("same")
	}

	fmt.Println(a <= b, b >= a, a != b)

	//SWITCH
	// implicit break, but if required can be used inside a case too
	// explicit fallthrough
	// syntax 1
	x := [...]int{1, 2, 3, 4, 5, 6, 7, 8}
	for i := range x {
		switch i {
		case 1, 5:
			fmt.Println("1 or 5")
		case 2, 6:
			fmt.Println("2 or 6")
		case 3, 7:
			fmt.Println("3 or 7")
		default:
			fmt.Println("-")
		}
	}

	// syntax 2 : Tagless
	j := 5
	switch {
	case j <= 10:
		fmt.Println("10")
		fallthrough // extra control, less use
	case j <= 20:
		fmt.Println("10 and 20")
	case j > 10 && j <= 20:
		fmt.Println("20")
	default:
		fmt.Println("20+")
	}

	// Typed switch
	var k interface{} = float32(1.) // interface can take in any form of datatype
	switch k.(type) {
	case int:
		fmt.Println("i is an int")
	case float64:
		fmt.Println("i is an float")
	case string:
		fmt.Println("i is an string")
	default:
		fmt.Println("not sure")
	}
}
