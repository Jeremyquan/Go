/*
Collection types
*/

package main

import (
	"fmt"
	"reflect" // to check the tab
)

type Details struct {
	name     string
	age      int
	location []string
}

// Embedding
// Go doesn support oops, it has no inheritence, instead it has composition
type Animal struct {
		name string
		origin string
	}

type Bird struct {
		canFly bool
		speed string
		Animal // to embed and say bir can have animal like things
	}

type MoreDetails struct { // for tagging
	name string `required max :"100"` // tag to restrcit the size of variable
	age int 
}

func main() {
	// MAPS are dictionary in python
	statePopulation := map[string]int{
		"TN": 12,
		"AP": 10,
		"KT": 13,
		"KL": 10,
	}
	fmt.Println(statePopulation)
	fmt.Println(statePopulation["TN"])

	m := map[[3]int]int{}
	fmt.Println(m)
	// a key can be any type which can be equal tested, so not a slice  or map

	statePop := make(map[string]int)
	statePop = map[string]int{
		"TN": 12,
		"AP": 10,
		"KT": 13,
		"KL": 10,
	}
	statePop["MH"] = 15 // appending a new KVP. But the ordering of the map is not guaranteed
	fmt.Println(statePop)
	delete(statePop, "MH") // removing a KVP
	fmt.Println(statePop)
	fmt.Println(statePop["MH"]) // this shows 0, infact any unavailable key will show 0
	// this confusion can be taken care of by a "comma ok" method
	pop, ok := statePop["MH"]
	fmt.Println(pop, ok) // this shows if the key is there or not
	pop2, ok := statePop["TN"]
	fmt.Println(pop2, ok)
	_, ok = statePop["TN"] // presence checker
	fmt.Println(ok)

	// passing maps and making changes can affect the original one
	sp := statePop
	fmt.Println(len(statePop))
	delete(sp, "TN")
	fmt.Println(len(sp))
	fmt.Println(len(statePop))

	// STRUCT
	// Like a user created data type made of premitives
	// only the type which can have hetrgeneous type in it
	detail1 := Details{
		name:     "Name1",
		age:      12,
		location: []string{"loc1", "loc2"},
	}
	fmt.Println(detail1)
	fmt.Println(detail1.location[0]) // drilling through the struct

	// anonymous structs
	struct2 := struct {
		name string
		age  int
	}{name: "someone", age: 23}
	fmt.Println(struct2)

	// unlike map and slices, passing structs will pass copies

	// Embedding example
	b := Bird{} // inittializing bird type to b, which has properties of animal
	b.name = "Bird1"
	b.origin = "Place1"
	b.canFly = true
	b.speed = "23 kmph"

	// the above one is not the literal way of value addition, its a syntactic sugar
	// to do the same, the structure of the struct has to be known

	b2 := Bird{ // inittializing bird type to b, which has properties of animal
	Animal : Animal {name :"Bird1",origin :"Place1"},
	canFly : true,
	speed : "23 kmph"}

	fmt.Println(b)
	fmt.Println(b2)

	// Embedding is not a good idea as it cant be used interchangably
	// SO INterfaces, later

	// Tag chcecking
	t := reflect.TypeOf(MoreDetails{})
	field ,_ := t.FieldByName("name")
	fmt.Println(field.Tag)


}

/*
map[TN:12 AP:10 KT:13 KL:10]
12
map[]
map[KL:10 MH:15 TN:12 AP:10 KT:13]
map[TN:12 AP:10 KT:13 KL:10]
0
0 false
12 true
true
4
3
3
{Name1 12 [loc1 loc2]}
loc1
{someone 23}
{true 23 kmph {Bird1 Place1}}
{true 23 kmph {Bird1 Place1}}
required max :"100"
 */
