package main

import (
	"fmt"
	"sync"
	//"time"
)

// writing for multiple channels, the variable, is not possible manually
// for simple system, channels are synced but that mght not be the case in complex ones
// by default channesl send and recive are blocking , the function has to get over before the next step executes

func foo(c chan int, someValue int) {
	defer wg.Done()
	c <- someValue * 5

}

var wg sync.WaitGroup

func main() {

	fooVal := make(chan int, 10) // for buffering 10 items

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go foo(fooVal, i)
	}

	wg.Wait()
	// to avoid error, clode all the channels
	close(fooVal) // but this is not synchronized, and value cant be sent thru closed channel

	for item := range fooVal {
		fmt.Println(item)
	}

	//time.Sleep(time.Second * 2)
}

/*

0
5
10
15
20
25
30
35
40
45
fatal error: all goroutines are asleep - deadlock!

goroutine 1 [chan receive]:
main.main()
        E:/Projects/Go/SentDex/22. Buffering Channels.go:25 +0x117
exit status 2

0
10
5
15
20
25
30
35
40
45

*/
