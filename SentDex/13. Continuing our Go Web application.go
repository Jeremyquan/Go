package main

import (
	"encoding/xml" // for parsing, builtin function, to "unmarshal"???
	"fmt"
	"io/ioutil"
	"net/http"
)

/*
If the site changes
var WashinPost = []byte(`
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<sitemap>
<loc>
http://www.washingtonpost.com/news-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-blogs-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-opinions-sitemap.xml
</loc>
</sitemap>`)
*/

// defining the structure of the sitemap, combining the previous structs and fucntion into one
type Sitemap struct {
	Locations []string `xml:"sitemap>loc"` // > loc inside the sitemap tag
}

// tyoe for news
type news struct {
	Titles    []string `xml:"url>news>title"`
	Keywords  []string `xml:"url>keywords"`
	Locations []string `xml:"url>loc"`
}

func main() {
	// reading the articles inside sitemaps
	// example politics has title and keywords from the xml file

	var s Sitemap
	var n News

	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	xml.Unmarshal(bytes, &s)

	// looping to get into the locations
	for i, Location := range s.Locations {
		resp, _ := http.Get(Location)
		bytes, _ := ioutil.ReadAll(resp.Body)
		xml.Unmarshal(bytes, &n)
		// the output which is title locaiton and title must be a KVP like a dictionary
	}

}

/*


 */
