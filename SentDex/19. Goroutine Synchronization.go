package main

import (
	"fmt"
	"sync"
	"time"
)

var waitGroup sync.WaitGroup // wait group type that is like a list of fucntions that can be scheduled

func say(s string) {
	for i := 0; i < 3; i++ {
		fmt.Println(s)
		time.Sleep(time.Millisecond * 100)
	}
	waitGroup.Done() // this notifies wait that go routine is over

	// if there is an error in the function and done is reached then the wait is for ever
}

func main() {
	waitGroup.Add(1) // addition of the go routine in the next line
	go say("Go something")
	waitGroup.Add(1)
	go say("here")
	waitGroup.Wait() // makes program wait for the go routine to finish

	// the following will be executed only if the first two are finished
	waitGroup.Add(1)
	go say("and there")
	waitGroup.Wait()
}

/*

here
Go something
Go something
here
here
Go something
and there
and there
and there

*/
