package main

import (
	"fmt"
	"time"
)

/* concurrancy : not in parallel
dealing with multiple things in parallel
not doing multiple things <- parallel

in web app created, we waste a lot of time waiting for response
sending out requests while waiting can shorten the time taken

This can be done using go routine
*/

func say(s string) {
	for i := 0; i < 3; i++ {
		fmt.Println(s)
		time.Sleep(time.Millisecond * 100)
	}
}

func main() {
	//say("Something")
	// to convert anything to a go routine just type go before the function call
	go say("Go something") // creates a light weight thread
	go say("here")

	// if all the function calls are go routined, there will be no output
	// if the program is over before the go routine does job, then it just doesn get executed
	// this can be avoided by providing some delay
	time.Sleep(time.Second)

	// this time harcoding is not practical, so synchronization can be done
}

/*

Somthing
Somthing
Somthing
here
Go something
here
Go something
here
Go something

*/
