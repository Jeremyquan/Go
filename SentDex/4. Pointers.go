package main

import (
	"fmt"
)

func main() {

	x := 15
	a := &x // & is a pointer, a has the memory address of x
	fmt.Println(x, a)
	fmt.Println(*a) // is used to print through the location
	*a = 5          // changing the value of x thourgh its pointer
	fmt.Println(x)
}

/*
15 0xc0420e090
15
5

*/
