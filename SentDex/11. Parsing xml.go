package main

import (
	"encoding/xml" // for parsing, builtin function, to "unmarshal"???
	"fmt"
	"io/ioutil"
	"net/http"
)

/*
If the site changes
var WashinPost = []byte(`
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<sitemap>
<loc>
http://www.washingtonpost.com/news-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-blogs-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-opinions-sitemap.xml
</loc>
</sitemap>`)
*/

// defining the structure of te sitemap
type Sitemap struct {
	Locations []Location `xml:"sitemap"` // Location is array type, to export it it has to be capitalized, with format of ``

}

type Location struct {
	Loc string `xml:"loc"`
}

// [] number in it is an array : [5 5]int => is 5 by 5 int array
// [] nothing in it is slice : []int

// since only values are fetched from the struct, value receiver would do

func (l Location) String() string { // this fucntion will always be sued to convert strcut kinda o/p to string
	return fmt.Sprintf(l.Loc)
}

func main() {

	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml") // ask for data from site
	bytes, _ := ioutil.ReadAll(resp.Body)                                        // read as bytes
	// bytes := WashinPost
	//fmt.Println(string(bytes))
	resp.Body.Close() // free up the resources

	var s Sitemap
	xml.Unmarshal(bytes, &s) // what to unmarshal and where to (into memory address of s)
	fmt.Println(s.Locations) // this will produce in a continuous way and not a formated string format
	// for that a method which applies string to it should be used
}

/*

[{http://www.washingtonpost.com/news-politics-sitemap.xml} {http://www.washingtonpost.com/news-blogs-politics-sitemap.xml} {http://www.washingtonpost.com/news-opinions-sitemap.xml} {http://www.washingtonpost.com/news-blogs-opinions-sitemap.xml} {http://www.washingtonpost.com/news-local-sitemap.xml} {http://www.washingtonpost.com/news-blogs-local-sitemap.xml} {http://www.washingtonpost.com/news-sports-sitemap.xml} {http://www.washingtonpost.com/news-blogs-sports-sitemap.xml} {http://www.washingtonpost.com/news-national-sitemap.xml} {http://www.washingtonpost.com/news-blogs-national-sitemap.xml} {http://www.washingtonpost.com/news-world-sitemap.xml} {http://www.washingtonpost.com/news-blogs-world-sitemap.xml} {http://www.washingtonpost.com/news-business-sitemap.xml} {http://www.washingtonpost.com/news-blogs-business-sitemap.xml} {http://www.washingtonpost.com/news-technology-sitemap.xml} {http://www.washingtonpost.com/news-blogs-technology-sitemap.xml} {http://www.washingtonpost.com/news-lifestyle-sitemap.xml} {http://www.washingtonpost.com/news-blogs-lifestyle-sitemap.xml} {http://www.washingtonpost.com/news-entertainment-sitemap.xml} {http://www.washingtonpost.com/news-blogs-entertainment-sitemap.xml} {http://www.washingtonpost.com/news-blogs-goingoutguide-sitemap.xml} {http://www.washingtonpost.com/news-goingoutguide-sitemap.xml}]
[http://www.washingtonpost.com/news-politics-sitemap.xml http://www.washingtonpost.com/news-blogs-politics-sitemap.xml http://www.washingtonpost.com/news-opinions-sitemap.xml http://www.washingtonpost.com/news-blogs-opinions-sitemap.xml http://www.washingtonpost.com/news-local-sitemap.xml http://www.washingtonpost.com/news-blogs-local-sitemap.xml http://www.washingtonpost.com/news-sports-sitemap.xml http://www.washingtonpost.com/news-blogs-sports-sitemap.xml http://www.washingtonpost.com/news-national-sitemap.xml http://www.washingtonpost.com/news-blogs-national-sitemap.xml http://www.washingtonpost.com/news-world-sitemap.xml http://www.washingtonpost.com/news-blogs-world-sitemap.xml http://www.washingtonpost.com/news-business-sitemap.xml http://www.washingtonpost.com/news-blogs-business-sitemap.xml http://www.washingtonpost.com/news-technology-sitemap.xml http://www.washingtonpost.com/news-blogs-technology-sitemap.xml http://www.washingtonpost.com/news-lifestyle-sitemap.xml http://www.washingtonpost.com/news-blogs-lifestyle-sitemap.xml http://www.washingtonpost.com/news-entertainment-sitemap.xml http://www.washingtonpost.com/news-blogs-entertainment-sitemap.xml http://www.washingtonpost.com/news-blogs-goingoutguide-sitemap.xml http://www.washingtonpost.com/news-goingoutguide-sitemap.xml]

*/
